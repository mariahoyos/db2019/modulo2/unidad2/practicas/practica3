<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "trabajadores".
 *
 * @property int $id
 * @property string $nombre
 * @property string $apellidos
 * @property string $fechaNacimiento
 * @property string $foto
 * @property int $delegacion
 *
 * @property Delegacion $delegacion0
 */
class Trabajadores extends \yii\db\ActiveRecord
{
    public $nombreCompleto;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trabajadores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fechaNacimiento'], 'safe'],
            [['delegacion'], 'integer'],
            [['nombre'], 'string', 'max' => 50],
            [['apellidos', 'foto'], 'string', 'max' => 100],
            [['delegacion'], 'exist', 'skipOnError' => true, 'targetClass' => Delegacion::className(), 'targetAttribute' => ['delegacion' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'fechaNacimiento' => 'Fecha Nacimiento',
            'foto' => 'Foto',
            'delegacion' => 'Delegacion',
            'nombreCompleto' => Yii::t('app', 'Nombre Completo')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDelegacion0()
    {
        return $this->hasOne(Delegacion::className(), ['id' => 'delegacion']);
    }
    
        
    /*public function afterFind() {
        parent::afterFind();
        $this->nombreCompleto = $this->nombre . ' ' . $this->apellidos;
    }
    
     public static function consulta1(){
        return \yii\helpers\ArrayHelper::map(self::find()->all(), "id", "nombreCompleto");
    }*/
    
    public static function consulta1(){
        return self::find()->select(["CONCAT_WS(nombre,apellidos) nombreCompleto"])
                ->all();
    }
    
}
