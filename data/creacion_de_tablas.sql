﻿DROP DATABASE IF EXISTS practica3yii;
CREATE DATABASE practica3yii;
USE practica3yii;

CREATE OR REPLACE TABLE delegacion(
  id int AUTO_INCREMENT,
  nombre varchar(50),
  direccion varchar(100),
  PRIMARY KEY(id)
);

CREATE OR REPLACE TABLE trabajadores(
  id int AUTO_INCREMENT,
  nombre varchar(50),
  apellidos varchar(100),
  fechaNacimiento date,
  foto varchar(100),
  delegacion int,
  PRIMARY KEY(id)
);

ALTER TABLE trabajadores
  ADD CONSTRAINT fk_trabajadores_delegacion
  FOREIGN KEY (delegacion) REFERENCES delegacion(id);