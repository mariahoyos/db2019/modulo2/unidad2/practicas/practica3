<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Trabajadores;
use app\models\Delegacion;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    public function actionConsulta12(){
        $todos_trabajadores = Trabajadores::find()->all();
        
        
        return $this->render('vardump',[
            'todos_trabajadores'=>$todos_trabajadores,
        ]);
    }
    
    public function actionConsulta14(){
        
        /* Todas las delegaciones ActiveRecord */
        $dataProvider = new ActiveDataProvider([
            'query' => Delegacion::find(),
        ]);
        
        /* Todas las delegaciones Command*/
        $resultado14 = Yii::$app->db
                ->createCommand("SELECT * FROM delegacion")->queryAll();
        return $this->render('vardump_14',[
            'resultado14'=>$resultado14,
        ]);
        
        
    }
    
    public function actionConsulta4(){
        //phpinfo();
        var_dump(Delegacion::find()->all());
    }
    
    public function actionConsulta17(){
        /* Todas las delegaciones de Santander ActiveRecord */
        $dataProvider = new ActiveDataProvider([
            'query' => Delegacion::find()->where("direccion = Santander"),
        ]);
        /* Todas las delegaciones de Santander Command*/
        $resultado17a = Yii::$app->db
                ->createCommand("SELECT * FROM delegacion WHERE direccion = 'Santander'")->queryAll();   
        /* Trabajadores de la delegación 1 */
        $dataProvider = new ActiveDataProvider([
            'query' => Delegacion::find()->where("id = 1"),
        ]);
        /* Trabajadores de la delegación 1 Command*/
        $resultado17b = Yii::$app->db
                ->createCommand("SELECT * FROM delegacion WHERE id =1")->queryAll();
        /* Todos las trabajadores ordenadas por nombre con ActiveRecord */
        $dataProvider = new ActiveDataProvider([
            'query' => Trabajadores::find()->orderBy("nombre"),
        ]);
        /* Todos los trabajadores ordenados por nombre con Command */
        $resultado17c = Yii::$app->db
                ->createCommand("SELECT * FROM trabajadores ORDER BY nombre")->queryAll();
        
        /* Todos los trabajadores que no tienen el nombre registrado con ActiveRecord */
        $dataProvider = new ActiveDataProvider([
            'query' => Trabajadores::find()->select("fechaNacimiento")->where("fechaNacimiento is NULL")
        ]);
        
        /* Todos los trabajadores que no tienen el nombre registrado con Command */
        $resultado17d = Yii::$app->db
                ->createCommand("SELECT * FROM trabajadores WHERE nombre IS NULL")->queryAll();
        
        
        
        
        return $this->render('consulta17',[
            'resultado17a'=>$resultado17a,
            'resultado17b'=>$resultado17b,
            'resultado17c'=>$resultado17c,
            'resultado17d'=>$resultado17d,
        ]);
        
    }
    
    public function actionConsulta20(){
        /* Listas las delegaciones que tengan una población distinta a Santander y de las cuales sepamos la dirección*/
        $resultado20a = Yii::$app->db
                ->createCommand("SELECT * FROM delegacion WHERE direccion <> 'Santander' AND direccion IS NOT NULL")->queryAll();
        /* Listar todos los trabajadores de las delegaciones de Santander */
        $resultado20b = Yii::$app->db
                ->createCommand("SELECT * FROM trabajadores WHERE delegacion = (SELECT id FROM delegacion WHERE direccion='Santander')")->queryAll();
        /* Listas todos los trabajadores y las delegaciones de los trabajadores que no tengan foto */
        $resultado20c = Yii::$app->db
                ->createCommand("SELECT * FROM trabajadores WHERE foto IS NULL")->queryAll();
        /* Sacar las delegaciones que no tengan trabajadores */ 
        $resultado20d = Yii::$app->db
                ->createCommand("SELECT delegacion FROM trabajadores GROUP BY delegacion HAVING COUNT(*)=0")->queryAll();
        
        return $this->render('consulta20', [
            'resultado20a'=>$resultado20a,
            'resultado20b'=>$resultado20b,
            'resultado20c'=>$resultado20c,
            'resultado20d'=>$resultado20d,  
        ]);
    }
    
    public function actionConsulta22(){
        
        $trabajadoresNombre= Trabajadores::consulta1();
        return $this->render('consulta22',[
            'trabajadoresNombre'=>$trabajadoresNombre,    
        ]);
    }
    
  
}
