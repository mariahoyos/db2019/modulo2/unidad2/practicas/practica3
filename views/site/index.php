<?php
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Práctica 3!</h1>

        <p class="lead">Módulo 2 - Unidad 2</p>

       <!-- <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Get started with Yii</a></p> -->
    </div> 

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Consulta 12</h2>

                <p>Listado de todos los trabajadores</p>

                <p><?= Html::a('Consulta 12', ['site/consulta12'], ['class' => 'btn btn-success']) ?></p>
            </div>
            <div class="col-lg-4">
                <h2>Consulta 14</h2>

                <p>Listado de todas las delegaciones</p>

                <p><?= Html::a('Consulta 14', ['site/consulta14'], ['class' => 'btn btn-success']) ?></p>
            </div>
            <div class="col-lg-4">
                <h2>Consulta 17</h2>
                
                <p>Listado de las delegaciones cuya población sea Santander</p>
                <p>Listado de los trabajadores de la delegación 1</p>
                <p>Listado de los trabajadores ordenados por el nombre</p>
                <p>Listado de las trabajadores que con fecha de nacimiento desconocida</p>

                <p><?= Html::a('Consulta 17', ['site/consulta17'], ['class' => 'btn btn-success']) ?></p>
            </div>
        </div>

    </div>
</div>
